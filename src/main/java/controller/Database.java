package controller;

import java.sql.*;
import java.util.ArrayList;

import model.CodeBuffer;

public class Database {
    
    static int i = 0;
    
    static ArrayList<Database> pool = new ArrayList<Database>();
    
    static { 
        try {
            Class.forName("org.sqlite.JDBC");
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
    private String file;
    private Connection conn;
    
    private Connection getConn() {
        try {
            if (conn == null || conn.isClosed()) {
                conn = DriverManager.getConnection("jdbc:sqlite:" + file + ".db");
                conn.setAutoCommit(false);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return conn;
    }
    
    public Database(String file) {
        
        pool.add(this);
        
        this.file = file;
        
    }
    
    public void clear() {
        try {
            Statement stmt = getConn().createStatement();
            String sql = "DROP TABLE IF EXISTS main.list;"
                       + "CREATE TABLE main.list ("
                       + "no   INT PRIMARY KEY,"
                       + "code BLOB"
                       + ");";
            stmt.executeUpdate(sql);
            conn.commit();
            stmt.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
    public void insert() {
        
        try {
            int cnt = 0;
            byte[] code;
            
            String sql = "INSERT INTO main.list VALUES(?, ?)";
            PreparedStatement prepInsertStmt = getConn().prepareStatement(sql);
            while (true) {
                code = CodeBuffer.codeBuffer.take();
                
                prepInsertStmt.setInt(1, ++i);
                prepInsertStmt.setBytes(2, code);
                prepInsertStmt.executeUpdate();
                cnt++;
                
                if (CodeBuffer.codeBuffer.peek() == null) {
                    conn.commit();
                }
                if (cnt >= 1000) {
                    cnt = 0;
                    conn.commit();
                }
            }
            
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
    public byte[] query(int no) {
        
        byte[] code = null;
        try {
            Statement stmt = getConn().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT code FROM main.list "
                    + "WHERE no = " + no);
            
            if (!rs.next())
                return null;
             code = rs.getBytes("code");
             rs.close();
             stmt.close();
        } catch (Exception e) {}
        return code;
    }
    
    public void importAll() {
        
        try {
            Statement stmt = getConn().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT code FROM main.list;");
            stmt.close();
            
            while (rs.next())
                CodeBuffer.codeBuffer.put(rs.getBytes("code"));
            rs.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
    public void close() {
        if (conn == null)
            return;
        try {
            conn.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
    public static void dispose() {
        for (Database i : pool)
            i.close();
    }

}