package controller;

import java.util.*;

import view.*;
import model.*;

import org.jnetpcap.*;
import org.jnetpcap.packet.*;

public class Packetcap {

    private static Pcap pcap;
    private static List<PcapIf> alldevs;
    private static int no;


    public static List<PcapIf> scanDevs() {

        alldevs = new ArrayList<PcapIf>();
        StringBuilder errbuf = new StringBuilder();

        int r = Pcap.findAllDevs(alldevs, errbuf);
        if (r == Pcap.NOT_OK || alldevs.isEmpty()) {

            return null;
        }

        return alldevs;
    }

    public static Pcap getPcap(List<PcapIf> alldevs, int n) {

        no = n;
        StringBuilder errbuf = new StringBuilder();
        PcapIf device = alldevs.get(no);

        int snaplen = 64 * 1024;
        int flags = Pcap.MODE_PROMISCUOUS;
        int timeout = 60 * 1000;
        pcap = Pcap.openLive(device.getName(), snaplen, flags, timeout, errbuf);

        if (pcap == null) {
            MainView.getMainView().state.setText("网卡连接失败");
            return null;
        }

        MainView.getMainView().state.setText("已连接");

        return pcap;
    }

    public static boolean startPcap() {

        if (pcap == null) {
            getPcap(alldevs, no);
        }

        if (pcap == null) {
            MainView.getMainView().state.setText("网卡连接失败");
            return false;
        }

        JPacketHandler<String> jpacketHandler = new JPacketHandler<String>() {

            @Override
            public void nextPacket(JPacket packet, String user) {

                byte[] code = packet.getByteArray(0, packet.size());

                try {
                    CodeBuffer.codeBuffer.put(code);
                } catch (InterruptedException e) {
                    System.out.println(e);
                }

                MainController.dealCaptureCode(code);

            }
        };

        new Thread() {
            public void run() {
                pcap.loop(-1, jpacketHandler, null);
            };
        }.start();
        return true;
    }

    public static boolean pause() {

        if (pcap != null) {
            pcap.breakloop();
            return true;
        }
        return false;
    }

    public static void dispose() {

        if (pcap != null) {
            pcap.close();
            MainView.getMainView().state.setText("未连接");
        }
    }
}
