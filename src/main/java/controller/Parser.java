package controller;

import model.*;


import org.jnetpcap.packet.format.FormatUtils;


public class Parser {

    private Parser() {
    }

    public static Result frameParser(byte[] code) {
        Result result = new Result();
        ethParser(result, code);
        return result;
    }

    private static void ethParser(Result result, byte[] code) {

        result.code = code;
        result.protocol = Protocol.ETH;
        result.eth = new Eth();

        // 解析目的MAC地址
        result.eth.dstMac = new String();
        for (int i = 0; i < 5; i++)
            result.eth.dstMac += (Formatter.byteToHexString(code[i]) + "-");
        result.eth.dstMac += Formatter.byteToHexString(code[5]);

        // 解析源MAC地址
        result.eth.srcMac = new String();
        for (int i = 6; i < 11; i++)
            result.eth.srcMac += (Formatter.byteToHexString(code[i]) + "-");
        result.eth.srcMac += Formatter.byteToHexString(code[11]);

        // 设置list中info
        result.info = "dstMac:" + result.eth.dstMac + "  srcMac:" + result.eth.srcMac;

        // 解析并且设置list中长度
        result.eth.length = code.length;
        result.length = result.eth.length;

        //设置src和dst
        result.src = result.eth.srcMac;
        result.dst = result.eth.dstMac;

        // 设置details
        result.details = "Ethernet II:\n" + FormatUtils.hexdump(code) + "\n\nDestination Mac:" + result.eth.dstMac
                + "\nSource Mac:" + result.eth.srcMac + "\nLength:" + result.length + "\n";

        // 解析数据类型
        byte[] data = new byte[code.length - 14];
        System.arraycopy(code, 14, data, 0, code.length - 14);

        int type = (code[12] & 0xff) << 8;
        type += code[13] & 0xff;
        if (type == 0x0806) {

            result.eth.dataProtocol = Protocol.ARP;
            arpParser(result, data);
        } else if (type == 0x8035) {

            result.eth.dataProtocol = Protocol.RARP;
            rarpParser(result, data);
        } else if (type == 0x0800) {

            result.eth.dataProtocol = Protocol.IP4;
            ip4Parser(result, data);
        } else if (type == 0x86dd) {

            result.eth.dataProtocol = Protocol.IP6;
            ip6Parser(result, data);
        }
    }

    private static void arpParser(Result result, byte[] code) {
        result.protocol = Protocol.ARP;
        result.arp = new Arp();

        //解析16位硬件地址类型
        int hardwareType = (code[0] & 0xff) << 8;
        hardwareType += code[1] & 0xff;
        if (hardwareType == 0x01)  //为1，代表以太网地址类型
            result.arp.hardwareType = "Ethernet (1)";

        //解析16位协议类型
        int protocolType = (code[2] & 0xff) << 8;
        protocolType += (code[3] & 0xff);
        if (protocolType == 0x0800)
            result.arp.protocolType = "IPv4 (0x0800)";

        //解析8位硬件地址长度
        result.arp.hardwareaddressLength = (code[4] & 0xff) + " bytes";

        //解析8位协议地址长度
        result.arp.protocoladdressLength = (code[5] & 0xff) + " bytes";

        //解析8位op
        int opCode = (code[6] & 0xff) << 8;
        opCode += code[7] & 0xff;
        if (opCode == 0x01)
            result.arp.opCode = "Request (1)";
        else if (opCode == 0x02)
            result.arp.opCode = "Replay (2)";

        //解析发送端以太网地址
        result.arp.srcMAC = result.eth.srcMac;

        //解析发送端IP地址
        result.arp.srcIP = new String();
        for (int i = 14; i < 17; i++) {
            int srcIP = code[i] & 0xff;
            result.arp.srcIP += srcIP + ".";
        }
        int srcIP = code[17] & 0xff;
        result.arp.srcIP += srcIP;

        //解析目的端以太网地址
        result.arp.dstMAC = result.eth.dstMac;

        //解析目的端IP地址
        result.arp.dstIP = new String();
        for (int i = 24; i < 27; i++) {
            int dstIP = code[i] & 0xff;
            result.arp.dstIP += dstIP + ".";
        }
        int dstIP = code[27] & 0xff;
        result.arp.dstIP += dstIP;


        //设置list中info
        result.src = result.arp.srcIP;
        result.dst = result.arp.dstIP;

        //设置list中的长度
        result.arp.length = result.eth.length - 14;
        result.length = result.arp.length;

        //设置list中details
        result.details += ("\nAddress Resolution Protocol:\n" + "Hardware Type:" + result.arp.hardwareType
                + "\nProtocol Type:" + result.arp.protocolType + "\nHardware Length:" + result.arp.hardwareaddressLength
                + "\nProtocol length:" + result.arp.protocoladdressLength + "\nOpcode:" + result.arp.opCode
                + "\nSender MAC address:" + result.arp.srcMAC + "\nSender IP address:" + result.arp.srcIP
                + "\nTarget MAC address:" + result.arp.dstMAC + "\nTarget IP address:" + result.arp.dstIP);


    }

    private static void rarpParser(Result result, byte[] code) {
        result.protocol = Protocol.RARP;
        result.rarp = new Rarp();

        //解析16位硬件地址类型
        int hardwareType = (code[0] & 0xff) << 8;
        hardwareType += code[1] & 0xff;
        if (hardwareType == 0x01)  //为1，代表以太网地址类型
            result.rarp.hardwareType = "Ethernet (1)";

        //解析16位协议类型
        int protocolType = (code[2] & 0xff) << 8;
        protocolType += (code[3] & 0xff);
        if (protocolType == 0x0800)
            result.rarp.protocolType = "IPv4 (0x0800)";

        //解析8位硬件地址长度
        result.rarp.hardwareaddressLength = (code[4] & 0xff) + " bytes";

        //解析8位协议地址长度
        result.rarp.protocoladdressLength = (code[5] & 0xff) + " bytes";

        //解析8位op
        int opCode = (code[6] & 0xff) << 8;
        opCode += code[7] & 0xff;
        if (opCode == 0x01)
            result.rarp.opCode = "Request (3)";
        else if (opCode == 0x02)
            result.rarp.opCode = "Replay (4)";

        //解析发送端以太网地址
        result.rarp.srcMAC = result.eth.srcMac;

        //解析发送端IP地址
        result.rarp.srcIP = new String();
        for (int i = 14; i < 17; i++) {
            int srcIP = code[i] & 0xff;
            result.rarp.srcIP += srcIP + ".";
        }
        int srcIP = code[17] & 0xff;
        result.rarp.srcIP += srcIP;

        //解析目的端以太网地址
        result.rarp.dstMAC = result.eth.dstMac;

        //解析目的端IP地址
        result.rarp.dstIP = new String();
        for (int i = 24; i < 27; i++) {
            int dstIP = code[i] & 0xff;
            result.rarp.dstIP += dstIP + ".";
        }
        int dstIP = code[27] & 0xff;
        result.rarp.dstIP += dstIP;

        //设置list中info
        result.src = result.rarp.srcIP;
        result.dst = result.rarp.dstIP;

        //设置list中的长度
        result.rarp.length = result.eth.length - 14;
        result.length = result.rarp.length;
        //设置details
        result.details += ("\nReverse Address Resolution Protocol:\n" + "Hardware Type:" + result.arp.hardwareType
                + "\nProtocol Type:" + result.arp.protocolType + "\nHardware Length:" + result.arp.hardwareaddressLength
                + "\nProtocol length:" + result.arp.protocoladdressLength + "\nOpcode:" + result.arp.opCode
                + "\nSender MAC address:" + result.arp.srcMAC + "\nSender IP address:" + result.arp.srcIP
                + "\nTarget MAC address:" + result.arp.dstMAC + "\nTarget IP address:" + result.arp.dstIP);

    }

    private static void ip4Parser(Result result, byte[] code) {

        result.protocol = Protocol.IP4;
        result.ip4 = new Ip4();

        //解析4位协议版本号与4位首部长度
        result.ip4.version = (code[0] & 0xf0) >> 4;
        result.ip4.headerLength = (code[0] & 0xff & 0x0f) * 4;

        //解析8位服务类型(TOS)
        result.ip4.tos = "0x" + (Formatter.byteToHexString(code[1]));

        //解析16位总长度
        int totalLength = (code[2] & 0xff) << 8;
        totalLength += code[3] & 0xff;
        result.ip4.totalLength = totalLength;

        //解析16位标识
        int identification = (code[4] & 0xff) << 8;
        identification += code[5] & 0xff;
        result.ip4.identification = identification;

        //解析3位标志  reservedBit,dontFragemnt,moreFragment
        int flag = code[6] & 0xe0;
        if (flag == 0x40) {          //"0 1 0"
            result.ip4.reservedBit = false;
            result.ip4.dontFragemnt = true;
            result.ip4.moreFragment = false;
        } else if (flag == 0x20) {     //"0 0 1"
            result.ip4.reservedBit = false;
            result.ip4.dontFragemnt = false;
            result.ip4.moreFragment = true;
        } else {                       //"0 0 0"
            result.ip4.reservedBit = false;
            result.ip4.dontFragemnt = false;
            result.ip4.moreFragment = false;
        }


        //解析13位片偏移
        int fragOffset = code[6] & 0xff & 0x1f;
        fragOffset <<= 8;
        fragOffset += code[7] & 0xff;
        result.ip4.fragOffset = fragOffset * 8;

        //解析8位生存时间
        result.ip4.ttl = code[8] & 0xff;

        //解析8位协议
        int protocolInip = code[9] & 0xff;
        if (protocolInip == 0x01)
            result.ip4.protocolInip = Protocol.ICMP;
        if (protocolInip == 0x02)
            result.ip4.protocolInip = Protocol.IGMP;
        if (protocolInip == 0x04)
            result.ip4.protocolInip = Protocol.IP4;
        if (protocolInip == 0x06)
            result.ip4.protocolInip = Protocol.TCP;
        if (protocolInip == 0x11)
            result.ip4.protocolInip = Protocol.UDP;
        if (protocolInip == 0x29)
            result.ip4.protocolInip = Protocol.IP6;

        //解析16位首部检验和
        int headerCRC = (code[10] & 0xff) << 8;
        headerCRC += code[11] & 0xff;
        result.ip4.headerCRC = headerCRC;

        //解析32位源IP地址
        result.ip4.srcIP = new String();
        for (int i = 12; i < 15; i++) {
            int srcIP = code[i] & 0xff;
            result.ip4.srcIP += srcIP + ".";
        }
        int srcIP = code[15] & 0xff;
        result.ip4.srcIP += srcIP;

        //解析32位目的IP地址
        result.ip4.dstIP = new String();
        for (int i = 16; i < 19; i++) {
            int dstIP = code[i] & 0xff;
            result.ip4.dstIP += dstIP + ".";
        }
        int dstIP = code[19] & 0xff;
        result.ip4.dstIP += dstIP;

        // 设置info
        result.info = "dstIP:" + result.ip4.dstIP + "  srcIP:" + result.ip4.srcIP;

        // 设置list中的长度
        result.length = result.ip4.totalLength;

        //设置list中的source 和Destination
        result.src = result.ip4.srcIP;
        result.dst = result.ip4.dstIP;


        // 解析数据类型
        byte[] data = new byte[code.length - result.ip4.headerLength];
        System.arraycopy(code, result.ip4.headerLength, data, 0, code.length - result.ip4.headerLength);

        result.details += ("\nInternet Protocol:\n" + "Version:" + result.ip4.version + "\nheader Length:" + result.ip4.headerLength + "bytes" +
                "\nTOS:" + result.ip4.tos + "\nTotal Length:" + result.ip4.totalLength + "\nIdentification:" + result.ip4.identification
                + "\nReserved Bit:" + result.ip4.reservedBit + "\nDon't Fragemnt:" + result.ip4.dontFragemnt
                + "\nMore Fragment:" + result.ip4.moreFragment + "\nFragment Offset:" + result.ip4.fragOffset + "bytes"
                + "\nTime To Live:" + result.ip4.ttl + "\nProtocol:" + result.ip4.protocolInip + "\nHeader Checksum:" + result.ip4.headerCRC
                + "\nSource IP:" + result.ip4.srcIP + "\nDestination IP:" + result.ip4.dstIP + "\n");

        //下一步解析
        if (result.ip4.protocolInip == Protocol.ICMP)
            icmpParser(result, data);

        if (result.ip4.protocolInip == Protocol.IGMP)
            igmpParser(result, data);
        if (result.ip4.protocolInip == Protocol.TCP)
            tcpParser(result, data);
        if (result.ip4.protocolInip == Protocol.UDP)
            udpParser(result, data);

    }

    private static void ip6Parser(Result result, byte[] code) {
        result.protocol = Protocol.IP6;
        result.ip6 = new Ip6();

        //解析4位版本
        result.ip6.version = new String();
        int version = (code[0] & 0xf0) >> 4;
        result.ip6.version += version;

        //解析8位优先级
        result.ip6.trafficClass = new String();
        int traffiClass = (code[0] & 0xff & 0x0f) << 4;
        traffiClass += code[1] & 0xf0;
        result.ip6.trafficClass += traffiClass;

        //解析20位流标号
        result.ip6.flowLabel = new String();
        int flowLabel = (code[1] & 0xff & 0x0f) << 16;
        flowLabel = (code[2] & 0xff) << 8;
        flowLabel += code[3] & 0xff;
        result.ip6.flowLabel += flowLabel;

        //解析16位有效符合长度
        result.ip6.payLoadlength = new String();
        int payLoadlength = (code[4] & 0xff) << 8;
        payLoadlength += code[5] & 0xff;
        result.ip6.payLoadlength += payLoadlength;

        //解析8位next header
        int nextHeader = code[6] & 0xff;

        if (nextHeader == 0x01)
            result.ip6.nextHeader = Protocol.ICMP;
        if (nextHeader == 0x02)
            result.ip6.nextHeader = Protocol.IGMP;
        if (nextHeader == 0x04)
            result.ip6.nextHeader = Protocol.IP4;
        if (nextHeader == 0x06)
            result.ip6.nextHeader = Protocol.TCP;
        if (nextHeader == 0x11)
            result.ip6.nextHeader = Protocol.UDP;


        //解析8位hop limit
        result.ip6.hopLimit = new String();
        int hopLimit = code[7] & 0xff;
        result.ip6.hopLimit += hopLimit;

        //解析128位源地址
        result.ip6.srcAddress += new String();
        for (int i = 8; i < 23; i++)
            result.ip6.srcAddress += (Formatter.byteToHexString(code[i]) + "-");
        result.ip6.srcAddress += Formatter.byteToHexString(code[23]);

        //解析128位目的地址
        result.ip6.dstAddress += new String();
        for (int i = 24; i < 39; i++)
            result.ip6.srcAddress += (Formatter.byteToHexString(code[i]) + "-");
        result.ip6.srcAddress += Formatter.byteToHexString(code[39]);


        // 设置info
        result.info = "dstIP:" + result.ip6.dstAddress + "  srcIP:" + result.ip6.srcAddress;

        // 设置list中的长度
        result.length = Integer.valueOf(result.ip6.payLoadlength);

        //设置list中的source 和Destination
        result.src = result.ip6.srcAddress;
        result.dst = result.ip6.dstAddress;


        // 解析数据类型
        byte[] data = new byte[Integer.valueOf(result.ip6.payLoadlength)];
        System.arraycopy(code, result.ip4.headerLength, data, 0, code.length - result.ip4.headerLength);

        result.details += ("\nInternet Protocol:\n" + "Version:" + result.ip6.version + "\nTraffic CLass:" + result.ip6.trafficClass
                + "\nFlow Label:" + result.ip6.flowLabel + "\nPayload Length:" + result.ip6.payLoadlength + "\nNext Header:" + result.ip6.nextHeader
                + "\nHop Limit:" + result.ip6.hopLimit + "\nSource Address:" + result.ip6.srcAddress + "\nDestination Address:" + result.ip6.dstAddress);

        //下一步解析
        if (result.ip6.nextHeader == Protocol.ICMP)
            icmpParser(result, data);
        if (result.ip6.nextHeader == Protocol.IGMP)
            igmpParser(result, data);
        if (result.ip6.nextHeader == Protocol.TCP)
            tcpParser(result, data);
        if (result.ip6.nextHeader == Protocol.UDP)
            udpParser(result, data);


    }

    private static void icmpParser(Result result, byte[] code) {
        result.protocol = Protocol.ICMP;
        result.icmp = new Icmp();

        //解析8位类型和8位代码
        int type = code[0] & 0xff;
        int icmpcode = code[1] & 0xff;

        if (type == 0x00) {
            result.icmp.type = type + " (Echo Reply)";
            if (icmpcode == 0x00)
                result.icmp.code = icmpcode + " (Echo Reply)";
        } else if (type == 0x03) {
            result.icmp.type = "Destination Unreachable";
            if (icmpcode == 0x00)
                result.icmp.code = icmpcode + " (Destination network unreachable)";
            if (icmpcode == 0x01)
                result.icmp.code = icmpcode + " (Destination host unreachable)";
            if (icmpcode == 0x02)
                result.icmp.code = icmpcode + " (Destination protocol unreachable)";
            if (icmpcode == 0x03)
                result.icmp.code = icmpcode + " (Destination port unreachable)";
            if (icmpcode == 0x04)
                result.icmp.code = icmpcode + " (Fragmentation required, and DF flag set)";
            if (icmpcode == 0x05)
                result.icmp.code = icmpcode + " (Source route failed)";
            if (icmpcode == 0x06)
                result.icmp.code = icmpcode + " (Destination network unknown)";
            if (icmpcode == 0x07)
                result.icmp.code = icmpcode + " (Destination host unknown)";
            if (icmpcode == 0x08)
                result.icmp.code = icmpcode + " (Source host isolated)";
            if (icmpcode == 0x09)
                result.icmp.code = icmpcode + " (Network administratively prohibited)";
            if (icmpcode == 0x0a)
                result.icmp.code = icmpcode + " (Host administratively prohibited)";
            if (icmpcode == 0x0b)
                result.icmp.code = icmpcode + " (Network unreachable for TOS)";
            if (icmpcode == 0x0c)
                result.icmp.code = icmpcode + " (Host unreachable for TOS)";
            if (icmpcode == 0x0d)
                result.icmp.code = icmpcode + " (Communication administratively prohibited)";
            if (icmpcode == 0x0e)
                result.icmp.code = icmpcode + " (Host Precedence Violation)";
            if (icmpcode == 0x0f)
                result.icmp.code = icmpcode + " (Precedence cutoff in effect)";
        } else if (type == 0x04) {
            result.icmp.type = type + " (Source Quench)";
            if (icmpcode == 0x00)
                result.icmp.code = icmpcode + " (Source quench (congestion control))";
        } else if (type == 0x05) {
            result.icmp.type = type + " (Redirect Message)";
            if (icmpcode == 0x00)
                result.icmp.code = icmpcode + " (Redirect Datagram for the Network)";
            if (icmpcode == 0x01)
                result.icmp.code = icmpcode + " (Redirect Datagram for the Host)";
            if (icmpcode == 0x02)
                result.icmp.code = icmpcode + " (Redirect Datagram for the TOS & network)";
            if (icmpcode == 0x03)
                result.icmp.code = icmpcode + " (Redirect Datagram for the TOS & host)";
        } else if (type == 0x08) {
            result.icmp.type = type + " (Echo)";
            if (icmpcode == 0x00)
                result.icmp.code = icmpcode + "(Echo request)";
        } else if (type == 0x09) {
            result.icmp.type = type + " (Router Advertisement)";
            if (icmpcode == 0x00)
                result.icmp.code = icmpcode + "(Router Advertisement)";
        } else if (type == 0x0a) {
            result.icmp.type = type + " (Router Solicitation)";
            if (icmpcode == 0x00)
                result.icmp.code = icmpcode + "(Router discovery/selection/solicitation)";
        } else if (type == 0x0b) {
            result.icmp.type = type + " (Time Exceeded)";
            if (icmpcode == 0x00)
                result.icmp.code = icmpcode + "(TTL expired in transit)";
            if (icmpcode == 0x01)
                result.icmp.code = icmpcode + "(Fragment reassembly time exceeded)";
        } else if (type == 0x0c) {
            result.icmp.type = type + " (Router Solicitation)";
            if (icmpcode == 0x00)
                result.icmp.code = icmpcode + "(Pointer indicates the error)";
            if (icmpcode == 0x01)
                result.icmp.code = icmpcode + "(Missing a required option)";
        } else if (type == 0x0d) {
            result.icmp.type = type + " (Timestamp)";
            if (icmpcode == 0x00)
                result.icmp.code = icmpcode + "(Timestamp)";
        } else if (type == 0x0e) {
            result.icmp.type = type + " (Timestamp Reply)";
            if (icmpcode == 0x00)
                result.icmp.code = icmpcode + "(Timestamp reply)";
        } else if (type == 0x0f) {
            result.icmp.type = type + " (Information Request)";
            if (icmpcode == 0x00)
                result.icmp.code = icmpcode + "(Information Request)";
        } else if (type == 0x10) {
            result.icmp.type = type + " (Information Reply)";
            if (icmpcode == 0x00)
                result.icmp.code = icmpcode + "(Information reply)";
        } else if (type == 0x11) {
            result.icmp.type = type + " (Address Mask Reques)";
            if (icmpcode == 0x00)
                result.icmp.code = icmpcode + "(Address mask request)";
        } else if (type == 0x12) {
            result.icmp.type = type + " (Address Mask Reply)";
            if (icmpcode == 0x00)
                result.icmp.code = icmpcode + "(Address mask reply)";
        } else if (type == 0x1e) {
            result.icmp.type = type + " (Traceroute)";
            if (icmpcode == 0x00)
                result.icmp.code = icmpcode + "(Information request)";
        }

        //解析16位检验和
        int checksum = (code[2] & 0xff) << 8;
        checksum += code[3];
        result.icmp.checksum = checksum + "";

        // 设置info
        result.info = "dstIP:" + result.ip4.dstIP + "  srcIP:" + result.ip4.srcIP;

        //设置list中source和destinaton
        result.src = result.ip4.srcIP;
        result.dst = result.ip4.dstIP;

        //设置list中legth
        result.icmp.length = result.ip4.totalLength - result.ip4.headerLength;
        result.length = result.icmp.length;

        result.details += ("\nInternet Control Message Protocol:\n" + "Type:" + result.icmp.type
                + "\nCode:" + result.icmp.code + "\nChecksum:" + result.icmp.checksum);

    }

    private static void igmpParser(Result result, byte[] code) {
        result.protocol = Protocol.IGMP;
        result.igmp = new Igmp();

        //版本
        result.igmp.version = 3 + "";

        //解析8位类型
        int type = code[0] & 0xff;
        if (type == 0x22)
            result.igmp.type = " Version 3 Membership Report" + "(0x11)";
        if (type == 0x11)    //为了兼容igmp v1
            result.igmp.type = " Membership Query" + "(0x11)";
        if (type == 0x12)    //为了兼容igmp v1
            result.igmp.type = " Version 1 Membership Report" + "(0x12)";
        if (type == 0x16)
            result.igmp.type = " Version 2 Membership Report" + "(0x16)";
        if (type == 0x17)
            result.igmp.type = " Leave group" + "(0x17)";

        //解析8位最大响应时间
        int maxResptime = code[1] & 0xff;
        result.igmp.maxResptime = maxResptime + "sec";

        //解析检验和
        int checksum = (code[2] & 0xff) << 8;
        checksum += code[3];
        result.igmp.chenkSum = checksum + "";

        //解析32位组播地址
        result.igmp.groupAddress = new String();
        for (int i = 4; i < 7; i++)
            result.igmp.groupAddress += (Formatter.byteToHexString(code[i]) + ".");
        result.igmp.groupAddress += Formatter.byteToHexString(code[7]);


        // 设置info
        result.info = "dstIP:" + result.ip4.dstIP + "  srcIP:" + result.ip4.srcIP;

        //设置list中source和destinaton
        result.src = result.ip4.srcIP;
        result.dst = result.ip4.dstIP;

        //设置list中length
        result.igmp.length = result.ip4.totalLength - result.ip4.headerLength;
        result.length = result.igmp.length;

        //设置details
        result.details += ("\nInternet Group Management Protocol:\n" + "IGMP Version:" + result.igmp.version
                + result.igmp.type + "\nMax Resp Time:" + result.igmp.maxResptime + "\nChecksum:" + result.igmp.chenkSum
                + "\nMulticast Address:" + result.igmp.groupAddress);

    }

    private static void tcpParser(Result result, byte[] code) {
        result.protocol = Protocol.TCP;
        result.tcp = new Tcp();


        //解析16位源端口号
        result.tcp.srcPort = new String();
        int srcPort = (code[0] & 0xff) << 8;
        srcPort += code[1] & 0xff;
        result.tcp.srcPort += srcPort;

        //解析16位目的端口号
        result.tcp.dstPort = new String();
        int dstPort = (code[2] & 0xff) << 8;
        dstPort += code[3] & 0xff;
        result.tcp.dstPort += dstPort;

        //解析32位序号
        result.tcp.sequenceNumber = new String();
        long sequenceNumber = 0;
        int move = 24;
        for (int i = 4; i < 8; i++) {
            sequenceNumber += ((long) (code[i] & 0xff)) << move;
            move -= 8;
        }
        result.tcp.sequenceNumber += sequenceNumber;

        //解析32位确认序号
        result.tcp.ackNumber = new String();
        long ackNumber = 0;
        move = 24;
        for (int i = 8; i < 12; i++) {
            ackNumber += ((long) (code[i] & 0xff)) << move;
            move -= 8;
        }
        result.tcp.ackNumber += ackNumber;

        //解析4位首部长度
        result.tcp.headerLength = new String();
        int headerlength = ((code[12] & 0xf0) >> 4) * 4;
        result.tcp.headerLength += headerlength;

        //解析6位保留位
        result.tcp.reserved = new String();
        int reserved = (code[12] & 0x0f) << 4;
        reserved += ((code[13] & 0xc0) >> 4);
        result.tcp.reserved += reserved;

        //解析URG、ACK、PSH、RST、SYN、FIN
        int urg = code[13] & 0x20;
        int ack = code[13] & 0x10;
        int psh = code[13] & 0x08;
        int rst = code[13] & 0x04;
        int syn = code[13] & 0x02;
        int fin = code[13] & 0x01;
        //urg
        if (urg == 0)
            result.tcp.urg = true;
        else
            result.tcp.urg = false;
        //ack
        if (ack == 0)
            result.tcp.ack = true;
        else
            result.tcp.ack = false;
        //psh
        if (psh == 0)
            result.tcp.psh = true;
        else
            result.tcp.psh = false;
        //rst
        if (rst == 0)
            result.tcp.rst = true;
        else
            result.tcp.rst = false;
        //syn
        if (syn == 0)
            result.tcp.syn = true;
        else
            result.tcp.syn = false;
        //fin
        if (fin == 0)
            result.tcp.fin = true;
        else
            result.tcp.fin = false;

        //解析16位窗口大小
        result.tcp.windowSizevalue = new String();
        int windowSizevalue = (code[14] & 0xff) << 8;
        windowSizevalue += code[15] & 0xff;
        result.tcp.windowSizevalue = windowSizevalue + " bytes";

        //解析16为检验和
        result.tcp.checkSum = new String();
        int checkSum = (code[16] & 0xff) << 8;
        checkSum += code[17] & 0xff;
        result.tcp.checkSum += checkSum;

        //解析16位紧急指针
        result.tcp.urgentPointer = new String();
        int urgentPointer = (code[18] & 0xff) << 8;
        urgentPointer += code[19] & 0xff;
        result.tcp.urgentPointer += urgentPointer;

        //设置list中的length

        result.length = result.ip4.totalLength - result.ip4.headerLength - headerlength;

        //解析tcp选项
        result.tcp.noOperation = new String();
        result.tcp.noOperationKind = new String();
        result.tcp.maxmumSegmentsize = new String();
        result.tcp.maxmumSegmentsizeKind = new String();
        result.tcp.maxmumSegmentsizeLength = new String();
        result.tcp.windowScale = new String();
        result.tcp.windowScaleKind = new String();
        result.tcp.windowScaleLength = new String();
        result.tcp.sackPermittedKind = new String();
        result.tcp.sackPermittedLength = new String();
        result.tcp.timeStamp = new String();
        result.tcp.timeStampKind = new String();
        result.tcp.timeStampLength = new String();
        result.tcp.timeStampreply = new String();

        boolean flag = false;
        int kind = 0;
        if (code.length - 1 >= 20) {
            flag = true;

            kind = code[20] & 0xff;

            if (kind == 0x02) {

                result.tcp.maxmumSegmentsizeKind = "maxmumSegmentsize " + kind;
                result.tcp.maxmumSegmentsizeLength += (code[21] & 0xff);
                int maxmumSegmentsize = (code[22] & 0xff) << 8;
                maxmumSegmentsize += code[23] & 0xff;
                result.tcp.maxmumSegmentsize = maxmumSegmentsize + " bytes";

            }

            if (kind == 0x01) {

                result.tcp.noOperationKind += kind;
                result.tcp.noOperation = "No-Operation " + kind;

            }


            if (kind == 0x03) {

                result.tcp.windowScaleKind = "Window scake " + kind;
                result.tcp.windowScaleLength += code[21] & 0xff;
                result.tcp.windowScale += code[22] & 0xff;

            }

            if (kind == 0x40) {

                result.tcp.sackPermittedKind = "SACK Permitted " + kind;
                result.tcp.sackPermittedLength += code[21] & 0xff;


            }
            if (kind == 0x08) {


                result.tcp.timeStampKind = "Time Stamp " + kind;
                result.tcp.timeStampLength += code[21] & 0xff;

                long timeStamp = 0;
                move = 24;
                for (int i = 22; i < 26; i++) {
                    timeStamp += ((long) (code[i] & 0xff)) << move;
                    move -= 8;
                }
                result.tcp.timeStamp += timeStamp;

                long timeStampreply = 0;
                move = 24;
                for (int i = 26; i < 30; i++) {
                    timeStampreply += ((long) (code[i] & 0xff)) << move;
                    move -= 8;
                }
                result.tcp.timeStampreply += timeStampreply;


            }
        }

        //设置list中的src、dst
        result.src = result.ip4.srcIP + ":" + result.tcp.srcPort;
        result.dst = result.ip4.dstIP + ":" + result.tcp.dstPort;

        //设置list中的info
        result.info = "dst:" + result.ip4.dstIP + ":" + result.tcp.dstPort + "  src:" + result.ip4.srcIP + ":" + result.tcp.srcPort;

        //设置details
        result.details += ("\nTransmission Control Protocol:\n" + "Source Port:" + result.tcp.srcPort + "\nDestination Port:"
                + result.tcp.dstPort + "\nSequence number:" + result.tcp.sequenceNumber + "\nAcknowledgment number:" + result.tcp.ackNumber
                + "\nHeader Length:" + result.tcp.headerLength + "bytes" + "\nReserved:" + result.tcp.reserved + "\nURG:" + result.tcp.urg
                + "\nACK:" + result.tcp.ack + "\nPSH:" + result.tcp.psh + "\nRST:" + result.tcp.rst + "\nSYN:" + result.tcp.syn + "\nFIN:" + result.tcp.fin
                + "\nWindow size value:" + result.tcp.windowSizevalue + "\nChecksum:" + result.tcp.checkSum + "\nUrgent pointer:" + result.tcp.urgentPointer);

        if (flag == true)
            if (kind == 0x01)
                result.details += ("\nOptions:\n" + "Kind:" + result.tcp.noOperation);
        if (kind == 0x02)
            result.details += ("\nOptions:\n" + "King:" + result.tcp.maxmumSegmentsizeKind + "\nLength:" + result.tcp.maxmumSegmentsizeLength
                    + "\nMaxmum Segment size:" + result.tcp.maxmumSegmentsize);
        if (kind == 0x03)
            result.details += ("\nOptions:\n" + "kind:" + result.tcp.windowScaleKind + "\nLength:" + result.tcp.windowScaleLength
                    + "\nWindow Sacle:" + result.tcp.windowScale);
        if (kind == 0x04)
            result.details += ("\nOptions:\n" + "Kind:" + result.tcp.sackPermittedKind + "\nLength:" + result.tcp.sackPermittedLength);
        if (kind == 0x08)
            result.details += ("\nOptions:\n" + "Kind:" + result.tcp.timeStampKind + "\nLength" + result.tcp.timeStampLength
                    + "\nTime Stamp:" + result.tcp.timeStamp + "\nTime Stamp Reply" + result.tcp.timeStampreply);


    }

    private static void udpParser(Result result, byte[] code) {
        result.protocol = Protocol.UDP;
        result.udp = new Udp();

        //解析16位源端口号
        result.udp.srcPort = new String();
        int srcPort = (code[0] & 0xff) << 8;
        srcPort += code[1] & 0xff;
        result.udp.srcPort += srcPort;

        //解析16位目的端口号
        result.udp.dstPort = new String();
        int dstPort = (code[2] & 0xff) << 8;
        dstPort += code[3] & 0xff;
        result.udp.dstPort += dstPort;

        //解析16位UDP长度
        result.udp.udpLength = new String();
        int udpLength = (code[4] & 0xff) << 8;
        udpLength += code[5] & 0xff;
        result.udp.udpLength += udpLength;

        //解析16位UDP检验和
        result.udp.checkSum = new String();
        int checkSum = (code[6] & 0xff) << 8;
        checkSum += code[7] & 0xff;
        result.udp.checkSum += checkSum;

        //设置list中的src 和 dst

        //设置list中的src、dst
        result.src = result.ip4.srcIP + ":" + result.udp.srcPort;
        result.dst = result.ip4.dstIP + ":" + result.udp.dstPort;

        //设置list中的info
        result.info = "dst:" + result.ip4.dstIP + ":" + result.udp.dstPort + "  src:" + result.ip4.srcIP + ":" + result.udp.srcPort;

        //设置details
        result.details += ("\nUser Datagram Protocol:\n" + "Source Port:" + result.udp.srcPort + "\nDestination Port:" + result.udp.dstPort
                + "\nLength:" + result.udp.udpLength + "\nChecksum:" + result.udp.checkSum);
    }
}

