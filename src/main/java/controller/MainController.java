package controller;

import javax.swing.*;
import javax.swing.table.*;

import view.MainView;
import view.SelectDataSource;
import model.*;

public class MainController {

    public static Database cache;
    public static Database input;
    public static Database output;
    
    public static int resultNo = 0;
    
    
    public static void dealCaptureCode(byte[] code) {
        
        Result result = Parser.frameParser(code);
        result.no = ++resultNo;
        updateList(result);

    }

    public static void dealStoredCode(byte[] code) {
        Result result = Parser.frameParser(code);
        updateDetails(result);
    }

    private static void updateList(Result result) {
        
        switch (result.protocol) {
            case ARP:
                if (!MainView.getMainView().arp.isSelected())
                    return;
                break;
            case RARP:
                if (!MainView.getMainView().rarp.isSelected())
                    return;
                break;
            case IP4:
                if (!MainView.getMainView().ip4.isSelected())
                    return;
                break;
            case IP6:
                if (!MainView.getMainView().ip6.isSelected())
                    return;
                break;
            case ICMP:
                if (!MainView.getMainView().icmp.isSelected())
                    return;
                break;
            case TCP:
                if (!MainView.getMainView().tcp.isSelected())
                    return;
                break;
            case UDP:
                if (!MainView.getMainView().udp.isSelected())
                    return;
                break;
            case HTTP:
                if (!MainView.getMainView().http.isSelected())
                    return;
                break;
            default:
                return;
        }
        
        
        Object[] record = new Object[6];
        record[0] = result.no;
        record[1] = result.protocol;
        record[2] = result.length;
        record[3] = result.src;
        record[4] = result.dst;
        record[5] = result.info;
        
        JTable list = MainView.getMainView().list;
        ((DefaultTableModel) list.getModel())
                .addRow(record);
        // Rectangle rect = new Rectangle(0, list.getHeight(), 1, 1);
        // list.scrollRectToVisible(rect);
    }

    

    private static void updateDetails(Result result) {
        MainView.getMainView().details.setText(result.details);
    }

    public static void main(String[] args) {
        new SelectDataSource();
    }
}
