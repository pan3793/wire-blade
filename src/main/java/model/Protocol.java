package model;

public enum Protocol {
    ETH, ARP, RARP, IP4, IP6, ICMP, IGMP, TCP, UDP, HTTP, UNKNOWN
}
