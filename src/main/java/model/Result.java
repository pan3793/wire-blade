package model;

public class Result {
    
    public int no;
    public Protocol protocol;
    public int length;
    public String src;
    public String dst;
    public byte[] code;
    public String info;
    public String details;
    
    public Eth  eth;
    public Arp  arp;
    public Rarp rarp;
    public Ip4  ip4;
    public Ip6  ip6;
    public Icmp icmp;
    public Igmp igmp;
    public Tcp  tcp;
    public Udp  udp;
    public Http http;
    
    public Result() {
        protocol = Protocol.UNKNOWN;
    }
    
    public Result(int mark) {
        no = 0;
        protocol = Protocol.UNKNOWN;
    }
    
    @Override
    public String toString() {
        return details;
    }
}
