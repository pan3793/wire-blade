package model;

public class Arp {
        public String hardwareType;
        public String protocolType;
        public String hardwareaddressLength;
        public String protocoladdressLength;
        public String opCode;
        public String srcIP;
        public String srcMAC;
        public String dstIP;
        public String dstMAC;
        public int length;
}
