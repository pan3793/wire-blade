package model;

public class Tcp {
    	public String srcPort;
    	public String dstPort;
    	public String sequenceNumber;
    	public String ackNumber;
    	public String headerLength;
    	public String reserved;
    	public boolean urg;
    	public boolean ack;
    	public boolean psh;
    	public boolean rst;
    	public boolean syn;
    	public boolean fin;
    	public String windowSizevalue;
    	public String checkSum;
    	public String urgentPointer;
    
    	public String maxmumSegmentsize;
    	public String maxmumSegmentsizeKind; 
    	public String maxmumSegmentsizeLength; 
    	public String noOperation;
     	public String noOperationKind;
    	public String windowScale;
    	public String windowScaleKind;
    	public String windowScaleLength;

    	public String sackPermittedKind;
    	public String sackPermittedLength;
    	
    	public String timeStamp;
    	public String timeStampKind;
    	public String timeStampLength;
    	public String timeStampreply;
    
    
    
    
}