package model;

public class Ip4 {
    
    
    public int version;
    public int headerLength;
    public String tos;
    public int totalLength;
    public int identification;
    public boolean reservedBit;
    public boolean dontFragemnt;
    public boolean moreFragment;
    public int  fragOffset;
    public int  ttl;
    public Protocol protocolInip;
    public int headerCRC;
    public String dstIP;
    public String srcIP;
    public byte[] data;  
    
    
}


