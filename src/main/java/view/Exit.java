package view;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import controller.Database;
import controller.Packetcap;

@SuppressWarnings("serial")
public class Exit extends JDialog {
    
    private void setUIFont() {
        Font font = new Font("微软雅黑", Font.PLAIN, 16);
        String names[] = { "Label", "CheckBox", "PopupMenu", "MenuItem",
                "CheckBoxMenuItem", "JRadioButtonMenuItem", "ComboBox",
                "Button", "Tree", "ScrollPane", "TabbedPane", "EditorPane",
                "TitledBorder", "Menu", "TextArea", "OptionPane", "MenuBar",
                "ToolBar", "ToggleButton", "ToolTip", "ProgressBar",
                "TableHeader", "Panel", "List", "ColorChooser",
                "PasswordField", "TextField", "Table", "Label", "Viewport",
                "RadioButtonMenuItem", "RadioButton", "DesktopPane",
                "InternalFrame" };
        for (String item : names) {
            UIManager.put(item + ".font", font);
        }
    }
    
    private void setSizeInCenter(int width, int height) {
        Toolkit kit = Toolkit.getDefaultToolkit();
        Dimension screenSize = kit.getScreenSize();
        int screenWeigt = screenSize.width;
        int screenHeight = screenSize.height;
        setBounds(screenWeigt / 2 - width / 2, screenHeight / 2 - height / 2, width, height);
    }
    
    public Exit() {
        
        setSizeInCenter(300, 150);
        setTitle("确认退出");
        setUIFont();
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        
        JPanel tip = new JPanel();
        tip.setBorder(new EmptyBorder(15, 10, 15, 10));
        tip.add(new JLabel("您确定要退出吗？"));
        add(tip, BorderLayout.CENTER);
        
        JButton exit = new JButton("退出");
        JButton cancel = new JButton("取消");
        
        Box mBox = Box.createHorizontalBox();
        mBox.setBorder(new EmptyBorder(5, 0, 5, 0));
        mBox.add(Box.createHorizontalGlue());
        mBox.add(exit);
        mBox.add(Box.createHorizontalGlue());
        mBox.add(cancel);
        mBox.add(Box.createHorizontalGlue());
        
        add(mBox, BorderLayout.SOUTH);
        
        exit.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent arg0) {
                
                Packetcap.dispose();
                Database.dispose();
                System.exit(0);
            }
        });
        
        cancel.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent arg0) {
                
                Exit.this.dispose();
            }
        });
        
        setVisible(true);
    }
}
