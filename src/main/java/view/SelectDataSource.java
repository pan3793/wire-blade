package view;

import java.awt.*;
import java.awt.event.*;
import java.util.List;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import org.jnetpcap.*;

import controller.*;

@SuppressWarnings("serial")
public class SelectDataSource extends JDialog {
    
    private void setUIFont() {
        Font font = new Font("微软雅黑", Font.PLAIN, 16);
        String names[] = { "Label", "CheckBox", "PopupMenu", "MenuItem",
                "CheckBoxMenuItem", "JRadioButtonMenuItem", "ComboBox",
                "Button", "Tree", "ScrollPane", "TabbedPane", "EditorPane",
                "TitledBorder", "Menu", "TextArea", "OptionPane", "MenuBar",
                "ToolBar", "ToggleButton", "ToolTip", "ProgressBar",
                "TableHeader", "Panel", "List", "ColorChooser",
                "PasswordField", "TextField", "Table", "Label", "Viewport",
                "RadioButtonMenuItem", "RadioButton", "DesktopPane",
                "InternalFrame" };
        for (String item : names) {
            UIManager.put(item + ".font", font);
        }
    }
    
    private void setSizeInCenter(int width, int height) {
        Toolkit kit = Toolkit.getDefaultToolkit();
        Dimension screenSize = kit.getScreenSize();
        int screenWeigt = screenSize.width;
        int screenHeight = screenSize.height;
        setBounds(screenWeigt / 2 - width / 2, screenHeight / 2 - height / 2, width, height);
    }
    
    public SelectDataSource() {
        
        setSizeInCenter(320, 400);
        setTitle("选择网卡");
        setUIFont();
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        
        this.addWindowListener(new WindowAdapter() {
            
            @Override
            public void windowClosing(WindowEvent e) {
                Packetcap.dispose();
            }
        });
        
        Integer flag = 0;
        
        JPanel selections = new JPanel();
        selections.setBorder(BorderFactory.createTitledBorder("请选择网卡"));
        
        Box selectionsBox = Box.createVerticalBox();
        
        List<PcapIf> devs = Packetcap.scanDevs();
        ButtonGroup group = new ButtonGroup();
        JRadioButton selection = null;
        // selection = new JRadioButton("本地导入");
        // selection.setName((flag++).toString());
        // group.add(selection);
        // selectionsBox.add(selection);
        
        for (PcapIf dev : devs) {
            String description = (dev.getDescription() != null) ? dev.getDescription() : "No description available";
            System.out.println(description);
            selection = new JRadioButton(description);
            selection.setName((flag++).toString());
            group.add(selection);
            selectionsBox.add(selection);
        }
        
        selections.add(selectionsBox);
        add(selections, BorderLayout.CENTER);
        
        JButton select = new JButton("选择");
        JButton exit = new JButton("退出");
        
        Box mBox = Box.createHorizontalBox();
        mBox.setBorder(new EmptyBorder(8, 0, 8, 0));
        mBox.add(Box.createHorizontalGlue());
        mBox.add(select);
        mBox.add(Box.createHorizontalGlue());
        mBox.add(exit);
        mBox.add(Box.createHorizontalGlue());
        
        add(mBox, BorderLayout.SOUTH);
        
        select.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent arg0) {
                
                Component[] coms = selectionsBox.getComponents();
                for (Component com : coms) {
                    if (com instanceof JRadioButton) {
                        JRadioButton selection = (JRadioButton) com;
                        if (selection.isSelected()) {
                            Packetcap.getPcap(devs, Integer.parseInt(selection.getName()));
                            SelectDataSource.this.dispose();
                            MainController.cache = new Database("cache");
                            MainController.cache.clear();
                            new Thread() {
                                public void run() {
                                    MainController.cache.insert();
                                }
                            }.start();
                            
                            MainView.getMainView();
                        }
                    }
                }
            }
        });
        
        exit.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent arg0) {
                Packetcap.dispose();
                System.exit(0);
            }
        });
        
        pack();
        setVisible(true);
        
    }
    
}
