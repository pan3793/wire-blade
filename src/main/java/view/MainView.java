package view;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.*;

import controller.*;

@SuppressWarnings("serial")
public class MainView extends JFrame {

    private static MainView mainView;

    public static MainView getMainView() {
        if (mainView == null)
            mainView = new MainView();
        return mainView;
    }

    public JTable list;

    public JTextArea details;

    public JLabel state;

    public JCheckBox arp;
    public JCheckBox rarp;
    public JCheckBox ip4;
    public JCheckBox ip6;
    public JCheckBox icmp;
    public JCheckBox tcp;
    public JCheckBox udp;
    public JCheckBox http;

    public JButton start;
    public JButton stop;
    public JButton output;

    private MainView() {
        init();
    }

    private void setUIFont() {
        Font font = new Font("微软雅黑", Font.PLAIN, 16);
        String names[] = { "Label", "CheckBox", "PopupMenu", "MenuItem",
                "CheckBoxMenuItem", "JRadioButtonMenuItem", "ComboBox",
                "Button", "Tree", "ScrollPane", "TabbedPane", "EditorPane",
                "TitledBorder", "Menu", "TextArea", "OptionPane", "MenuBar",
                "ToolBar", "ToggleButton", "ToolTip", "ProgressBar",
                "TableHeader", "Panel", "List", "ColorChooser",
                "PasswordField", "TextField", "Table", "Label", "Viewport",
                "RadioButtonMenuItem", "RadioButton", "DesktopPane",
                "InternalFrame" };
        for (String item : names) {
            UIManager.put(item + ".font", font);
        }
    }

    private void setSizeInCenter(int width, int height) {
        Toolkit kit = Toolkit.getDefaultToolkit();
        Dimension screenSize = kit.getScreenSize();
        int screenWeigt = screenSize.width;
        int screenHeight = screenSize.height;
        setBounds(screenWeigt / 2 - width / 2, screenHeight / 2 - height / 2,
                width, height);
    }

    private void init() {

        setUIFont();
        setTitle("网络数据包分析工具");
        setSizeInCenter(1280, 720);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

        this.addWindowListener(new WindowAdapter() {
            
            @Override
            public void windowClosing(WindowEvent e) {
                
                if (Packetcap.pause()) {
                    stop.setEnabled(false);
                    start.setEnabled(true);
                    output.setEnabled(true);
                }
                new Exit();
            }
        });

        initList();
        initDetails();
        initctrl();

        setVisible(true);
    }

    private void initList() {

        DefaultTableModel tableModel = new DefaultTableModel() {

            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        tableModel.addColumn("No.");
        tableModel.addColumn("Protocol");
        tableModel.addColumn("Length");
        tableModel.addColumn("Source");
        tableModel.addColumn("Desination");
        tableModel.addColumn("Info");

        list = new JTable(tableModel);
        list.setFont(new Font("Consolas", 1, 14));

        int[] widths = new int[] { 80, 80, 100, 220, 220, 600 };
        for (int i = 0; i < 5; i++)
            list.getColumnModel().getColumn(i).setMinWidth(widths[i]);
        for (int i = 0; i < 5; i++)
            list.getColumnModel().getColumn(i).setMaxWidth(widths[i]);

        JScrollPane listPane = new JScrollPane();
        listPane.setPreferredSize(new Dimension(1280, 400));
        listPane.setViewportView(list);
        add(listPane, BorderLayout.NORTH);

        list.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {

                DefaultTableModel dtm = (DefaultTableModel) list.getModel();
                int no = (int) dtm.getValueAt(list.getSelectedRow(), 0);
                byte[] code = MainController.cache.query(no);
                MainController.dealStoredCode(code);
            }
        });
    }

    private void initDetails() {

        details = new JTextArea();
        details.setFont(new Font("Consolas", 1, 14));
        details.setEditable(false);

        JScrollPane detailsPane = new JScrollPane();
        detailsPane.setViewportView(details);
        add(detailsPane, BorderLayout.CENTER);
    }

    private void initctrl() {

        state = new JLabel("未连接");

        arp = new JCheckBox("ARP");
        rarp = new JCheckBox("RARP");
        ip4 = new JCheckBox("IPv4");
        ip6 = new JCheckBox("IPv6");
        icmp = new JCheckBox("ICMP");
        tcp = new JCheckBox("TCP");
        udp = new JCheckBox("UDP");
        http = new JCheckBox("HTTP");

        arp.setSelected(true);
        rarp.setSelected(true);
        ip4.setSelected(true);
        ip6.setSelected(true);
        icmp.setSelected(true);
        tcp.setSelected(true);
        udp.setSelected(true);
        http.setSelected(true);

        start = new JButton("开始");
        stop = new JButton("停止");
        output = new JButton("输出");
        
        start.setEnabled(true);
        stop.setEnabled(false);
        output.setEnabled(false);

        Box mBox = Box.createHorizontalBox();
        mBox.setBorder(new EmptyBorder(5, 5, 5, 5));
        mBox.add(state);
        mBox.add(Box.createHorizontalGlue());
        mBox.add(new JLabel("请选择协议类型："));
        mBox.add(arp);
        mBox.add(Box.createHorizontalStrut(10));
        mBox.add(rarp);
        mBox.add(Box.createHorizontalStrut(10));
        mBox.add(ip4);
        mBox.add(Box.createHorizontalStrut(10));
        mBox.add(ip6);
        mBox.add(Box.createHorizontalStrut(10));
        mBox.add(icmp);
        mBox.add(Box.createHorizontalStrut(10));
        mBox.add(tcp);
        mBox.add(Box.createHorizontalStrut(10));
        mBox.add(udp);
        mBox.add(Box.createHorizontalStrut(10));
        mBox.add(http);
        mBox.add(Box.createHorizontalGlue());
        mBox.add(start);
        mBox.add(Box.createHorizontalStrut(10));
        mBox.add(stop);
        mBox.add(Box.createHorizontalStrut(10));
        mBox.add(output);
        add(mBox, BorderLayout.SOUTH);

        start.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                if (Packetcap.startPcap()) {
                    start.setEnabled(false);
                    stop.setEnabled(true);
                    output.setEnabled(false);
                }
            }
        });

        stop.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                if (Packetcap.pause()) {
                    stop.setEnabled(false);
                    start.setEnabled(true);
                    output.setEnabled(true);
                }
            }
        });
        
        output.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e) {
                
                
            }
        });
        
        
        
    }

}
