plugins {
    `java-library`
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

version = "1.0.0-SNAPSHOT"

repositories {
    maven {
        url = uri("https://maven.aliyun.com/repository/public")
    }
    maven {
        url = uri("https://clojars.org/repo/")
    }
}

tasks.withType<JavaCompile>() {
    options.encoding = "UTF-8"
}

dependencies {
    implementation("jnetpcap:jnetpcap:1.4.r1425-1g")
    implementation("org.xerial:sqlite-jdbc:3.30.1")
    implementation("com.ardikars.pcap:pcap-common:0.0.8")
}
